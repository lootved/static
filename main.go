package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

func main() {

	port := "80"
	dir := "."
	pub := false

	flag.StringVar(&port, "p", port, "port to listen to")
	flag.StringVar(&dir, "d", dir, "directory to serve")
	flag.BoolVar(&pub, "pub", pub, "serve on a public interface")
	flag.Parse()

	fs := http.FileServer(http.Dir(dir))
	http.Handle("/", fs)

	itf := "127.0.0.1"
	if pub {
		itf = "0.0.0.0"
	}
	addr := itf + ":" + port
	log.Println("Listening on " + addr)
	err := http.ListenAndServe(addr, nil)
	if err != nil {
		fmt.Println("sudo setcap 'cap_net_bind_service=+ep' <path/to/binary> to allow bind to low port")
		log.Fatal(err)
	}
}
